package com.liferay.pinnacle.portlet;

//import static com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet._log;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.ProcessAction;

import org.osgi.service.component.annotations.Component;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.pinnacle.constants.EmployeePortletKeys;
import com.liferay.pinnacle.model.employee;
import com.liferay.pinnacle.service.employeeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;

/**
 * @author Kamesh
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EmployeePortletKeys.Employee,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class EmployeePortlet extends MVCPortlet {
	String pattern = "yyyy-MM-dd";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	
	public void employeeSubmit(ActionRequest request,ActionResponse response)
	{
	
		String name=ParamUtil.getString(request, "name");
		Date dob=ParamUtil.getDate(request, "dob", simpleDateFormat);
		Date doj=ParamUtil.getDate(request, "doj" ,simpleDateFormat);
		String qualification=ParamUtil.getString(request, "qualification");
		String experience=ParamUtil.getString(request, "experience");
		
		System.out.println(name + ""+dob +"" +doj +"" +qualification +""+experience );
		
		employee employee1=employeeLocalServiceUtil.createemployee(CounterLocalServiceUtil.increment());
		employee1.setEmployeeName(name);
        employee1.setDob(dob);
        employee1.setDoj(doj);
        employee1.setQualification(qualification);
        employee1.setExperince(experience);
		employeeLocalServiceUtil.addemployee(employee1);
		
	}
	@ProcessAction(name="updateEmploye")
	public void updateemployee(ActionRequest actionRequest, ActionResponse actionResponse) {
        //_log.info("am inside of the empUpdate method");
        Long employeid = ParamUtil.getLong(actionRequest, "employeeId");
      //  String emplyename = ParamUtil.getString(actionRequest, "EmplyeName");
        String name=ParamUtil.getString(actionRequest, "employeeName");
        Date dob=ParamUtil.getDate(actionRequest, "dob", simpleDateFormat);
        Date doj=ParamUtil.getDate(actionRequest, "doj" ,simpleDateFormat);
      
        String qualification = ParamUtil.getString(actionRequest, "qualification");
        String experience=ParamUtil.getString(actionRequest, "experience");
        
     
        employeeLocalServiceUtil.updateemployee(employeid, name, dob, doj, experience, qualification);
    }

public void  EmpDelete(ActionRequest actionRequest,ActionResponse acResponse) throws PortalException {

     

        long employeid = ParamUtil.getLong(actionRequest, "employeeId");

       
        employeeLocalServiceUtil.deleteemployee(employeid);

        SessionMessages.add(actionRequest, "success");

    }
	
}