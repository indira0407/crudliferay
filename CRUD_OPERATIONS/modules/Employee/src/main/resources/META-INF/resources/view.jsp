<%@page import="com.liferay.pinnacle.service.employeeLocalServiceUtil"%>
<%@page import="com.liferay.pinnacle.service.employeeLocalService"%>
<%@page import ="com.liferay.pinnacle.model.employee" %>
<%@ include file="/init.jsp" %>
<%@page import="javax.portlet.RenderResponse"%>

<%@page import="javax.portlet.PortletURL"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.pinnacle.model.employee" %>

<%
    List<employee> dd= employeeLocalServiceUtil.getemployees(-1, -1);
    System.out.println(dd  +"view.jsp page");
    
    
%> 
<p>
	<b><liferay-ui:message key="employee.caption"/></b>
</p>
<portlet:actionURL name ="employeeSubmit" var="employeeSubmit" />
<h1>Employee Form</h1>
<form action="<%=employeeSubmit %>" method="post">
Name:<input type="text" name="<portlet:namespace/>name"><br>
DOB:<input type="date" name="<portlet:namespace/>dob"><br>
DOJ:<input type="date" name="<portlet:namespace/>doj"><br>
Qualifaction:<input type="text" name="<portlet:namespace/>qualification"><br>
Experience:<input type="text" name="<portlet:namespace/>experience"><br>
<input type="submit" value="submit">



</form>
    

<liferay-ui:search-container delta="10" emptyResultsMessage="NoPage">    
    <liferay-ui:search-container-results  results="<%=ListUtil.subList(dd, searchContainer.getStart(), searchContainer.getEnd()) %>"/>
    <liferay-ui:search-container-row className="employee" modelVar="employee" keyProperty="employeeId">
    
    <portlet:renderURL var="updateURL">
            <portlet:param name="jspPage" value="/update.jsp" />
             <portlet:param name="employeeId" value="${employee.employeeId}"/>
    </portlet:renderURL>
    <portlet:actionURL name="EmpDelete" var="DeleteURL" >
             <portlet:param name="employeeId" value="${employee.employeeId}"/>
    </portlet:actionURL>
    
        <liferay-ui:search-container-column-text name="employeeId" property="employeeId"/>
        <liferay-ui:search-container-column-text name="Name" property="employeeName"/>
        <liferay-ui:search-container-column-text name="dob" property="dob"/>
        <liferay-ui:search-container-column-text name="doj" property="doj"/>
        <liferay-ui:search-container-column-text name="qualification" property="qualification"/>
        <liferay-ui:search-container-column-text name="experince" property="experince"/>
        <liferay-ui:search-container-column-text name="Update" href="${updateURL}" value="Update" >
        </liferay-ui:search-container-column-text>
        <liferay-ui:search-container-column-text name="Delete" href="${DeleteURL}" value="Delete" >
        </liferay-ui:search-container-column-text>
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"/>
</liferay-ui:search-container>