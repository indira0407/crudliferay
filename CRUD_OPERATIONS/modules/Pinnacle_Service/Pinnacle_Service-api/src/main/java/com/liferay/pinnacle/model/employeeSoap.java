/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.pinnacle.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.liferay.pinnacle.service.http.employeeServiceSoap}.
 *
 * @author Indira Thevar
 * @see com.liferay.pinnacle.service.http.employeeServiceSoap
 * @generated
 */
@ProviderType
public class employeeSoap implements Serializable {
	public static employeeSoap toSoapModel(employee model) {
		employeeSoap soapModel = new employeeSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setEmployeeId(model.getEmployeeId());
		soapModel.setEmployeeName(model.getEmployeeName());
		soapModel.setDob(model.getDob());
		soapModel.setDoj(model.getDoj());
		soapModel.setQualification(model.getQualification());
		soapModel.setExperince(model.getExperince());

		return soapModel;
	}

	public static employeeSoap[] toSoapModels(employee[] models) {
		employeeSoap[] soapModels = new employeeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static employeeSoap[][] toSoapModels(employee[][] models) {
		employeeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new employeeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new employeeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static employeeSoap[] toSoapModels(List<employee> models) {
		List<employeeSoap> soapModels = new ArrayList<employeeSoap>(models.size());

		for (employee model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new employeeSoap[soapModels.size()]);
	}

	public employeeSoap() {
	}

	public long getPrimaryKey() {
		return _employeeId;
	}

	public void setPrimaryKey(long pk) {
		setEmployeeId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getEmployeeId() {
		return _employeeId;
	}

	public void setEmployeeId(long employeeId) {
		_employeeId = employeeId;
	}

	public String getEmployeeName() {
		return _employeeName;
	}

	public void setEmployeeName(String employeeName) {
		_employeeName = employeeName;
	}

	public Date getDob() {
		return _dob;
	}

	public void setDob(Date dob) {
		_dob = dob;
	}

	public Date getDoj() {
		return _doj;
	}

	public void setDoj(Date doj) {
		_doj = doj;
	}

	public String getQualification() {
		return _qualification;
	}

	public void setQualification(String qualification) {
		_qualification = qualification;
	}

	public String getExperince() {
		return _experince;
	}

	public void setExperince(String experince) {
		_experince = experince;
	}

	private String _uuid;
	private long _employeeId;
	private String _employeeName;
	private Date _dob;
	private Date _doj;
	private String _qualification;
	private String _experince;
}