/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.pinnacle.service.impl;

import java.util.Date;

import com.liferay.pinnacle.model.employee;
import com.liferay.pinnacle.model.impl.employeeImpl;
import com.liferay.pinnacle.service.employeeLocalServiceUtil;
import com.liferay.pinnacle.service.base.employeeLocalServiceBaseImpl;

/**
 * The implementation of the employee local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.pinnacle.service.employeeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Indira Thevar
 * @see employeeLocalServiceBaseImpl
 * @see com.liferay.pinnacle.service.employeeLocalServiceUtil
 */
public class employeeLocalServiceImpl extends employeeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.liferay.pinnacle.service.employeeLocalServiceUtil} to access the employee local service.
	 */
	
	public  employee updateemployee(Long employeeId,String employeeName,Date dob,Date doj,String experince,String qualification) {
        
        employee employee = new employeeImpl();
        if(employeeId > 0l){
        	employee = employeeLocalServiceUtil.fetchemployee(employeeId);
        }
        employee.setEmployeeId(employeeId);
        employee.setEmployeeName(employeeName);
        employee.setDob(dob);
        employee.setExperince(experince);
        employee.setQualification(qualification);
        if(employeeId > 0l){
        	employee = updateemployee(employee);
        }
        return employee;
        
    }
}