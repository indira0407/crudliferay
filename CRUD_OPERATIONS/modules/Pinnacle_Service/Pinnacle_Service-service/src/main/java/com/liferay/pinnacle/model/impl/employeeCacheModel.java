/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.pinnacle.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.pinnacle.model.employee;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing employee in entity cache.
 *
 * @author Indira Thevar
 * @see employee
 * @generated
 */
@ProviderType
public class employeeCacheModel implements CacheModel<employee>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof employeeCacheModel)) {
			return false;
		}

		employeeCacheModel employeeCacheModel = (employeeCacheModel)obj;

		if (employeeId == employeeCacheModel.employeeId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, employeeId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", employeeId=");
		sb.append(employeeId);
		sb.append(", employeeName=");
		sb.append(employeeName);
		sb.append(", dob=");
		sb.append(dob);
		sb.append(", doj=");
		sb.append(doj);
		sb.append(", qualification=");
		sb.append(qualification);
		sb.append(", experince=");
		sb.append(experince);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public employee toEntityModel() {
		employeeImpl employeeImpl = new employeeImpl();

		if (uuid == null) {
			employeeImpl.setUuid("");
		}
		else {
			employeeImpl.setUuid(uuid);
		}

		employeeImpl.setEmployeeId(employeeId);

		if (employeeName == null) {
			employeeImpl.setEmployeeName("");
		}
		else {
			employeeImpl.setEmployeeName(employeeName);
		}

		if (dob == Long.MIN_VALUE) {
			employeeImpl.setDob(null);
		}
		else {
			employeeImpl.setDob(new Date(dob));
		}

		if (doj == Long.MIN_VALUE) {
			employeeImpl.setDoj(null);
		}
		else {
			employeeImpl.setDoj(new Date(doj));
		}

		if (qualification == null) {
			employeeImpl.setQualification("");
		}
		else {
			employeeImpl.setQualification(qualification);
		}

		if (experince == null) {
			employeeImpl.setExperince("");
		}
		else {
			employeeImpl.setExperince(experince);
		}

		employeeImpl.resetOriginalValues();

		return employeeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		employeeId = objectInput.readLong();
		employeeName = objectInput.readUTF();
		dob = objectInput.readLong();
		doj = objectInput.readLong();
		qualification = objectInput.readUTF();
		experince = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(employeeId);

		if (employeeName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(employeeName);
		}

		objectOutput.writeLong(dob);
		objectOutput.writeLong(doj);

		if (qualification == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(qualification);
		}

		if (experince == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(experince);
		}
	}

	public String uuid;
	public long employeeId;
	public String employeeName;
	public long dob;
	public long doj;
	public String qualification;
	public String experince;
}